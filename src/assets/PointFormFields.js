export default [
    {
        label: 'Nombre del punto',
        handler: 'pointName',
        type: 'input',
    },
    {
        label: 'Estado',
        handler: 'pointState',
        type: 'input',
    },
    {
        label: 'Ciudad',
        handler: 'pointCity',
        type: 'input',
    },
    {
        label: 'Municipio',
        handler: 'pointMunicipality',
        type: 'input',
    },
    {
        label: 'Parroquia',
        handler: 'pointParish',
        type: 'input',
    },
    {
        label: 'Dirección',
        handler: 'pointAddress',
        type: 'input',
    },
    {
        label: 'Tipo de punto',
        handler: 'pointType',
        type: 'select',
        list: [
            { value: '01', label: 'Bodegón' }, 
            { value: '02', label: 'Farmacia' }, 
            { value: '03', label: 'Casa de Familia' }, 
            { value: '04', label: 'Supermercado' }, 
            { value: '05', label: 'Agente Autorizado' }, 
            { value: '06', label: 'Librería' }, 
            { value: '07', label: 'Ferreteria' }, 
            { value: '08', label: 'Centro de Apuesta' }, 
            { value: '09', label: 'Kiosko' },
            { value: '10', label: 'Lunchería' },
            { value: '11', label: 'Panadería' },
            { value: '12', label: 'Otro' },
        ],
    },
    {
        label: 'Tamaño',
        handler: 'pointSize',
        type: 'select',
        list: [
            { value: '1', label: 'Pequeño' },
            { value: '2', label: 'Mediano' },
            { value: '3', label: 'Grande' },
        ],
    },
    {
        label: 'Cadena',
        handler: 'pointChain',
        type: 'radio',
        list: [
            { id: '0', value: '0', label: 'No' },
            { id: '1', value: '1', label: 'Sí' },
        ],
    },
    {
        label: 'Captado',
        handler: 'pointCaught',
        type: 'radio',
        list: [
            { id: '0', value: '0', label: 'No' },
            { id: '1', value: '1', label: 'Sí' },
        ],
    },
]