import { Alert, Linking, PermissionsAndroid, Platform, ToastAndroid } from "react-native";
import appConfig from '../../app.json';
import { clearToken } from "../store/reducers";

const D2R = Math.PI / 180.0; // value used for converting degrees to radians
const toRadians = (num) => num * D2R;
const distance = (center0, center1) => {
    // convert degrees to radians
    const r0 = {
      latitude: toRadians(center0.latitude),
      longitude: toRadians(center0.longitude),
    };
    const r1 = {
      latitude: toRadians(center1.latitude),
      longitude: toRadians(center1.longitude),
    };

    // calculate the differences for both latitude and longitude (the deltas)
    const dltLat = r1.latitude - r0.latitude;
    const dltLng = r1.longitude - r0.longitude;

    // calculate the great use haversine formula to calculate great-circle distance between two points
    const a = Math.pow(Math.sin(dltLat / 2), 2) + Math.pow(Math.sin(dltLng / 2), 2) * Math.cos(r0.latitude) * Math.cos(r1.latitude);
    const c = 2 * Math.asin(Math.sqrt(a));
    const d = c * 6378137;  // multiply by the radius of the great-circle (average radius of the earth in meters)

    return d;
};
export const hasIntersections = (circle0, circle1, r, currentRadius) => {
    const maxDist = r + currentRadius;
    const actualDist = distance(circle0, circle1);

    return maxDist >= actualDist;
};

export const dataFromGeocoding = (json) => {
    const {
        sublocality,
        route,
        state,
        municipality,
        city,
        parish
    } = json.results.reduceRight((addressData, currAddrComp, i, arr) => {
        const reducedAddrs = currAddrComp.address_components.reduce((reducedData, currAddrs) => {
            // console.log('the data: ', JSON.stringify(reducedData));
            const setCond = (key) => {
                let str = currAddrs.long_name || currAddrs.short_name;
                const pattern = new RegExp(`${key} `, 'gi');
                if (pattern.test(str)) str = str.replace(pattern, '');
                reducedData[key] = str;
            };
            const conds = [
                ['administrative_area_level_1','estado'],
                ['administrative_area_level_2','municipio'],
                ['locality','ciudad'],
                ['administrative_area_level_3','parroquia'],
                ['sublocality'],
                ['route']
            ];
            for (let j = 0; j < conds.length; j++) if (currAddrs.types.includes(conds[j][0])) { setCond(conds[j][conds[j].length - 1]); break; }

            return reducedData;
        }, {});

        addressData = {
            ...addressData,
            ...(reducedAddrs.estado ? { state: reducedAddrs.estado } : {}),
            ...(reducedAddrs.municipio ? { municipality: reducedAddrs.municipio } : {}),
            ...(reducedAddrs.ciudad ? { city: reducedAddrs.ciudad } : {}),
            ...(reducedAddrs.parroquia ? { parish: reducedAddrs.parroquia } : {}),
            ...(reducedAddrs.sublocality ? { sublocality: reducedAddrs.sublocality } : {}),
            ...(reducedAddrs.route ? { route: reducedAddrs.route } : {}),
        };

        return addressData;
    }, {});

    return {
        sublocality,
        route,
        state,
        municipality,
        city,
        parish
    };
};

// FOR IOS
const hasPermissionIOS = async (geolocation) => {
    const openSetting = () => {
      Linking.openSettings().catch(() => {
        Alert.alert('No se pueden abrir las configuraciones');
      });
    };
    const status = await geolocation.requestAuthorization('whenInUse');

    if (status === 'granted') {
      return true;
    }

    if (status === 'denied') {
      Alert.alert('Permiso de ubicación denegado');
    }

    if (status === 'disabled') {
      Alert.alert(
        `Habilita los servicios de ubicación para permitir que "${appConfig.displayName}" determine tu ubicación.`,
        '',
        [
          { text: 'Dirigete a Configuraciones', onPress: openSetting },
          { text: "No usar Ubicación", onPress: () => {} },
        ],
      );
    }

    return false;
};

// GENERAL USE
export const hasLocationPermission = async (geolocation) => {
    if (Platform.OS === 'ios') {
      const hasPermission = await hasPermissionIOS(geolocation);
      return hasPermission;
    }

    if (Platform.OS === 'android' && Platform.Version < 23) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) {
      return true;
    }

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) {
      return true;
    }

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Permiso de ubicación denegado por el usuario.',
        ToastAndroid.LONG,
      );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Permiso de ubicación revocado por el usuario.',
        ToastAndroid.LONG,
      );
    }

    return false;
};

export const dataConstructor = (values, formFields, token, form) => {
  const {
    // POINT
    pointAddress: direccionPV,
    pointCaught: captado,
    pointChain: estatusCadena,
    pointCity: codigoCiudad,
    pointMunicipality: codigoMunicipio,
    pointName: nombrePV,
    pointParish: codigoParroquia,
    pointSize: tamanoPV,
    pointState: codigoEstado,
    pointType: codigoTipoPV,
    // LOGIN
    login, 
    password: clave,
  } = values;

  const {
      latitude,
      longitude,
  } = formFields?.formCoords || {};

  const tokenUsuario = token || '';

  return {
      ...(
          form === 'point' ? {
              tokenUsuario,
              latitudPV: `${latitude}`,
              longitudPV: `${longitude}`,
              direccionPV,
              captado,
              estatusCadena,
              codigoCiudad,
              codigoMunicipio,
              nombrePV,
              codigoParroquia,
              tamanoPV,
              codigoEstado,
              codigoTipoPV,
          } : 
          form === 'login' ? {
              login/* : "19817395" */,
              clave/* : "123abc" */,
          } : 
          {}
      )
  };
}

export const executeCloseSession = (dispatch, nav) => {
  dispatch(clearToken());
  if (nav) nav.reset({
      index: 0,
      routes: [{ name: 'Login' }],
  });
}