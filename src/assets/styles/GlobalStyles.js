import { Dimensions, Platform, StyleSheet } from "react-native";

export const { width, height } = Dimensions.get('window');

export const COLORS = {
    black: ['black', 'rgba(0,0,0,0.5)'],
    red: ['red', 'rgba(255,0,0,0.5)'],
    green: ['#63E104', 'rgba(99,225,4,0.5)'],
    blue: ['#1E75D1', 'rgba(30,117,209,0.5)'],
};

export const DURATION = 500;

export const styles_ = () => StyleSheet.create({
    rowCenter: {
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    result: {
        borderWidth: 1,
        borderColor: '#666',
        width: '100%',
        padding: 10,
    },
    container: {
        ...(Platform.OS === 'android' ? {
            height: '100%',
        } : {
            height: height,
            ...StyleSheet.absoluteFillObject,
        })
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    textMarker: {
        color: 'black', 
        fontWeight: 'bold', 
    },
    floatingBtnContainer: {
        ...(Platform.OS === 'android' ? StyleSheet.absoluteFillObject : {}),
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between'
    },
    floatingBtn: {
        borderRadius: 8,
        margin: 8,
        paddingVertical: 8,
        paddingHorizontal: 10,
        elevation: 2,
    },
    flexEnd: {
        alignSelf: 'flex-end',
    },
    flexStart: {
        alignSelf: 'flex-start',
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 15,
        shadowColor: '#000',
        shadowOffset: {
        width: 0,
        height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: width - 15,
        flex: 1,
        ...(Platform.OS === 'android' ? {
            marginVertical: 7.5,
        } : {}),
    },
    modalViewHeader: {
        // backgroundColor: 'cyan',
        alignItems: 'flex-end',
    },
    modalViewBody: {
        justifyContent: 'center',
        flex: 1,
        // backgroundColor: 'pink',
    },
    timesCloser: { 
        color: COLORS.black[1] || COLORS.black[0] ,
        fontSize: 28,
        lineHeight: 24
    },
    titleInModal: {
        color: 'black', 
        fontWeight: 'bold', 
        fontSize: 15,
    },
    inputInModal: {
        color: 'black', 
        borderWidth: 1, 
        borderColor: COLORS.black[1] || COLORS.black[0], 
        borderRadius: 8,
        paddingVertical: 4,
        paddingHorizontal: 16,
        fontSize: 15,
    },
    separatorTop: { 
        marginTop: 10,
    },
    textInBtn: {
        color: 'white', 
        textAlign: 'center',
    },
    btnInModal: {
        backgroundColor: COLORS.blue[0],
        color: 'white',
        padding: 10,
        borderRadius: 8,
        marginTop: 20,
    },
    radioContainer: {
        alignItems: 'center', 
        flexDirection:'row',
    },
    pickerContainer: {
        paddingVertical: 0, 
        height: 40,
        paddingHorizontal: 0,
    },
    errorMsg: {
        fontSize: 10,
        color: 'red',
    },
    loginContainer: {
        backgroundColor: 'white',
        height: '100%',
        justifyContent: 'center',
        padding: 15,
    },
    bigLabel: {
        color: 'black',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 26,
        marginBottom: 15,
    },
    separatorInner: {
        width: 5,
    },
    whiteColor: {
        color: 'white',
    },
    pickerModal: {
        height: 200,
        padding: 15,
        backgroundColor: '#f0f0f0',
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        marginHorizontal: 7.5,
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20,
        overflow: 'visible',
        shadowColor: 'black',
        shadowOffset: {width: 0, height: -4},
        shadowOpacity: 0.15,
    },
    pickerInModal: {
        height: 150,
    },
});