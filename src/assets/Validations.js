import * as Yup from 'yup';

export const validations = (form, isIos) => Yup.object({
    ...(
        // POINT
        form === 'point' ? {
            ...[
                'Name',
                'State',
                'City',
                'Municipality'
            ].reduce(
                (keys, key) => 
                keys = { 
                    ...keys, 
                    [`point${key}`]: Yup.string().required('Requerido')
                                        .max(
                                            150, 
                                            ({max}) => `Solo se permiten ${max} caracteres`
                                        )
                                        .matches(
                                            /^[aA-zZñÑÀ-ÿ\s]+$/, 
                                            "Ingrese solo letras"
                                        ) 
                }, 
                {}
            ),
            ...(isIos ? {
                pointTypeInput: Yup.string().required('Requerido').notOneOf(['Seleccione'], 'Debe seleccionar una opción'),
                pointSizeInput: Yup.string().required('Requerido').notOneOf(['Seleccione'], 'Debe seleccionar una opción'),
            } : {}),
            pointParish: Yup.string().required('Requerido')
                            .max(
                                150, 
                                ({max}) => `Solo se permiten ${max} caracteres`
                            )                
                            .matches(
                                /^[aA-zZñÑÀ-ÿ0-9\s]+$/, 
                                "Ingrese solo letras y/o números"
                            ),
            pointAddress: Yup.string().required('Requerido')
                            .max(
                                150, 
                                ({max}) => `Solo se permiten ${max} caracteres`
                            )
                            .matches(
                                /^[aA-zZñÑÀ-ÿ0-9,.-\s]+$/, 
                                "Ingrese solo letras, números y/o caracteres especiales (coma, punto, guión)"
                            ),
        } 
        // LOGIN
        : form === 'login' ? [
            'login',
            'password'
        ].reduce(
            (keys, key, i) => 
            keys = { 
                ...keys, 
                [key]: Yup.string().required('Requerido')
                        .min(
                            6, 
                            ({min}) => `debe tipear mínimo ${min} caracteres`
                        )        
                        .max(
                            !i ? 10 : 12, 
                            ({max}) => `Solo se permiten ${max} caracteres`
                        )
                        .matches(
                            /^[aA-zZ0-9]+$/, 
                            "Ingrese solo letras y/o números sin espacios en blanco"
                        )
            }, 
            {}
        ) 
        : 
        {}
    ),
})