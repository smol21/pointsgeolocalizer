export default [
    {
        label: 'Login',
        handler: 'login',
        type: 'input',
    },
    {
        label: 'Clave',
        handler: 'password',
        type: 'input',
    },
]