import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { HomeScreen } from '../views/HomeScreen';

const Drawer = createDrawerNavigator();

export const DrawerNavigator = ({
  DrawerContent,
}) => {
  return (
    <Drawer.Navigator 
      initialRouteName="Home"
      screenOptions={{ headerShown: false }} 
      useLegacyImplementation={true}
      drawerContent={(props) => <DrawerContent {...props} />}
    >
      <Drawer.Screen name="Home" options={{ title: 'Inicio' }} component={HomeScreen} />
    </Drawer.Navigator>
  );
};
