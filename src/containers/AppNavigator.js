import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { LoginScreen } from '../views/LoginScreen';
// import { DrawerNavigator } from './DrawerNavigator';
// import { CustomDrawerContent } from './CustomDrawerContainer';
import { useSelector } from 'react-redux';
import { HomeScreen } from '../views/HomeScreen';

// const DrawerNavigatorContainer = () => <DrawerNavigator DrawerContent={CustomDrawerContent} />;

const Stack = createNativeStackNavigator();

export const AppNavigator = () => {
  const token = useSelector((state) => state.auth.token);
  // <Stack.Screen name="Drawer" component={DrawerNavigatorContainer} />
  // HomeScreen is TEMPORAL
  return (
    <Stack.Navigator initialRouteName={!token ? "Login" : /* "Drawer" */"Home"} screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Login" component={LoginScreen} />
      <Stack.Screen name="Home" component={HomeScreen} />
    </Stack.Navigator>
  );
};