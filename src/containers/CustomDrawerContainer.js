import React from "react";
import { DrawerContentScrollView, DrawerItem, DrawerItemList } from "@react-navigation/drawer";
import { Text, View } from "react-native";
import { useDispatch } from "react-redux";
import { executeCloseSession } from "../assets/Utils";

export const CustomDrawerContent = (props) => {
    const dispatch = useDispatch();

    return (
        <View style={{flex: 1}}>
            {/* <View>
                <Text style={{color: 'black'}}>HEADER</Text>
            </View> */}
            <DrawerContentScrollView {...props}>
                <DrawerItemList {...props} />
                <DrawerItem label="Cerrar Sesión" onPress={() => {
                    if (props?.navigation) executeCloseSession(dispatch, props.navigation);
                }} />
            </DrawerContentScrollView>
            {/* <View>
                <Text style={{color: 'black'}}>HEADER</Text>
            </View> */}
        </View>
    );
};