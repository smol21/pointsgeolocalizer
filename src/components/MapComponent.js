import React, { useEffect, useRef, useState } from 'react';
import { Alert, Text, View } from 'react-native';
import MapView, { Callout, Circle, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { COLORS, DURATION, height, styles_, width } from '../assets/styles/GlobalStyles';
import { hasIntersections } from '../assets/Utils';

const ASPECT_RATIO = width / height;
const LATITUDE = 9.031087;
const LONGITUDE = -66.606372;
const LATITUDE_DELTA = 0.003;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const MIN_ZOOM = 4;
const MAX_ZOOM = 18;

// MICRO COMPONENTS
const customText = (text, title = false) => <Text style={title ? styles.textMarker : { color: styles.textMarker.color}}>{text}</Text>;

export const MapComponent = ({
    location, 
    isDarkMode, 
    circles = [],
    getStrokeColor = COLORS.green[1] || COLORS.green[0],
    onSetDidIntersect = () => {},
    callGeocodeFrom = () => {},
    callLocation = () => {},
}) => {
    const [region, setRegion] = useState({
        altitude: 15000,
        center: {
          latitude: LATITUDE,
          longitude: LONGITUDE,
        },
        heading: 0,
        pitch: 0,
        zoom: MIN_ZOOM,
    });
    const [minZoom, setMinZoom] = useState(MIN_ZOOM);
    const [currentRadius, setCurrentRadius] = useState(30);
    const [strokeColors, setStrokeColors] = useState(getStrokeColor);
    const [didIntersect, setDidIntersect] = useState(false);
    const [animateCam, setAnimateCam] = useState(true);
    const [showRadius, setShowRadius] = useState(false);

    const mapRef = useRef(null);  
    
    const setMinZoomFunc = () => {
      setTimeout(() => {
        setMinZoom(MAX_ZOOM);
      }, DURATION);
    };

    const setMarkerLocation = (e) => {
        if (e?.nativeEvent?.coordinate) {
          const hasIntrs = circles.some(circle => hasIntersections(circle, e.nativeEvent.coordinate, circle.radius, currentRadius));
          setDidIntersect(hasIntrs);
          onSetDidIntersect(hasIntrs);
          if (mapRef.current && animateCam) {
            // setAnimateCam(false);
            mapRef.current.animateCamera({
              center: {
                latitude: e.nativeEvent.coordinate.latitude,
                longitude: e.nativeEvent.coordinate.longitude,
              },
              pitch: 0,
              heading: 0,
              altitude: 1000,
              zoom: MAX_ZOOM,
            }, {
              duration: DURATION,
            });
          }
          setRegion({
            ...region, 
            center: {
              latitude: e.nativeEvent.coordinate.latitude,
              longitude: e.nativeEvent.coordinate.longitude,
            },
            zoom: minZoom,
          });
          setStrokeColors(hasIntrs ? (COLORS.red[1] || COLORS.red[0]) : (COLORS.green[1] || COLORS.green[0]));
          if (hasIntrs) Alert.alert('Ésta área ya ha sido tomada');

          setMinZoomFunc();
        }
    }

    const sttCallLocation = () => {
      const dur = (DURATION * 3) - (DURATION / 2);
      setTimeout(callLocation, dur);
    }

    // USE EFFECT
    useEffect(() => {
        if (location?.coords?.latitude && location?.coords?.longitude) {
          const {
            coords: {
              latitude,
              longitude,
            }
          } = location;

          setMarkerLocation({ // Object formatted to be passed
            nativeEvent: {
              coordinate: {
                latitude,
                longitude,
              }
            }
          });
        }

        setShowRadius(true);
    },[location]);

    useEffect(() => {callGeocodeFrom(region)}, [region]);

    return (
        <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            ref={mapRef}
            initialCamera={region}
            minZoomLevel={minZoom}
            // loadingEnabled
            // loadingBackgroundColor="white"
            scrollEnabled={false}
            onMapReady={() => {sttCallLocation()}}
        >
          {circles.map((circle, ci) => 
            <View key={ci}>
              {false ? <Circle
                center={{
                  longitude: circle.longitude,
                  latitude: circle.latitude
                }}
                radius={circle.radius}
                strokeWidth={5}
                strokeColor={COLORS.black[1] || COLORS.black[0]}
              /> : <></>}
              <Marker
                  userInterfaceStyle={isDarkMode ? 'dark' : 'light'}
                  coordinate={{
                    longitude: circle.longitude,
                    latitude: circle.latitude
                  }}
              >
                <Callout>
                  <View>
                    {circle?.info?.name.length ? customText(circle.info.name, true) : ''}
                  </View>
                </Callout>
              </Marker>
            </View>
          )}
          {location ? <>
            {/* showRadius */false ? <Circle
              center={region.center}
              radius={currentRadius}
              strokeWidth={5}
              strokeColor={strokeColors}
            /> : <></>}
            <Marker
                draggable
                userInterfaceStyle={isDarkMode ? 'dark' : 'light'}
                coordinate={region.center}
                onDragEnd={setMarkerLocation}
            >
              <Callout>
                <View>
                  {customText('Posición actual', true)}
                  {customText(`Latitud: ${region.center.latitude}`)}
                  {customText(`Longitud: ${region.center.longitude}`)}
                </View>
              </Callout>
            </Marker>
          </>
          : <></>}
        </MapView>
    );
};

const styles = styles_();