import React, { useState } from "react";
import { styles_ } from "../assets/styles/GlobalStyles";
import {
    Platform,
    TextInput,
    Modal,
    View,
    Pressable,
    Text,
} from 'react-native';
import { Picker, PickerIOS } from "@react-native-picker/picker";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { TouchableOpacity } from "react-native-gesture-handler";

export const PickerComponent = ({
    field,
    values,
    touched,
    errors,
    isSubmitting,
    setFieldValue = () => {},
}) => {
    const [modalVisible, setModalVisible] = useState({});
    const insets = useSafeAreaInsets();
    const [closeModalEnabled, setCloseModalEnabled] = useState(true);

    return (
        <>
        {Platform.OS === 'ios' ? 
            <>
                <TouchableOpacity onPress={() => {
                    if (!isSubmitting) {
                        setModalVisible({ ...modalVisible, [field.handler]: true });
                        if (!values[field.handler]) {
                            setFieldValue(field.handler, field.list[0].value);
                            setFieldValue(`${field.handler}Input`, field.list[0].label);
                        }   
                    }
                }}>
                    <TextInput
                        editable={false}
                        style={styles.inputInModal}
                        value={values[`${field.handler}Input`]}
                    />
                    {touched[`${field.handler}Input`] && errors[`${field.handler}Input`] ? <Text style={styles.errorMsg}>{errors[`${field.handler}Input`]}</Text> : <></>}
                </TouchableOpacity>
                <Modal
                    animationType="slide"
                    visible={modalVisible[field.handler] ? true : false}
                    onRequestClose={() => {
                        setModalVisible({ ...modalVisible, [field.handler]: false })
                    }}
                    transparent={true}
                >
                    <View style={{
                        ...styles.pickerModal,
                        marginBottom: insets.bottom + 7.5,
                    }}>
                        <View style={styles.modalViewHeader}>
                            <Pressable disabled={!closeModalEnabled} onPress={() => setModalVisible({ ...modalVisible, [field.handler]: false })}>
                                <Text style={styles.timesCloser}>&times;</Text>
                            </Pressable>
                        </View>
                        <PickerIOS
                            selectedValue={values[field.handler]}
                            onValueChange={(value, ind) => {
                                setFieldValue(field.handler, value);
                                setFieldValue(`${field.handler}Input`, field.list[ind].label);
                            }}
                            itemStyle={styles.pickerInModal}
                        >
                            {field.list.map((itm, i) => <PickerIOS.Item key={i} label={itm.label} value={itm.value} />)}
                        </PickerIOS>
                    </View>
                </Modal>
            </>
            : 
            <View style={[styles.inputInModal, styles.pickerContainer]}>
                <Picker
                    style={{
                        color: 'black',
                        marginTop: -8,
                    }}
                    enabled={!isSubmitting}
                    prompt="Seleccione"
                    dropdownIconColor="black"
                    selectedValue={values[field.handler]}
                    onValueChange={(itemValue, itemIndex) => setFieldValue(field.handler, itemValue)}
                >
                    {field.list.map((itm, i) => <Picker.Item key={i} label={itm.label} value={itm.value} />)}
                </Picker>
            </View>
        }
        </>
    );
};

const styles = styles_();