import React, { useEffect, useRef, useState } from 'react';
import { Formik } from 'formik';
import { Pressable, ScrollView, Text, TextInput, View, Platform, Alert, ActivityIndicator } from 'react-native';
import { COLORS, styles_ } from '../assets/styles/GlobalStyles';
import { RadioButton } from 'react-native-radio-buttons-group';
import { validations } from '../assets/Validations';
import { apiFetch } from '../services/ApisCenter';
import { useDispatch, useSelector } from 'react-redux';
import { dataConstructor, executeCloseSession } from '../assets/Utils';
import { PickerComponent } from './PickerComponent';
import { useNavigation } from '@react-navigation/native';

export const FormComponent = ({
    formFields,
    fieldsArray,
    btnLabel,
    form = 'point', // or 'login'
    handleAction = () => {},
    setModalActionsEnabled = () => {},
}) => {
    const navigation = useNavigation();
    const inputRefs = {
        ...[...(form === 'point' 
            ? ['pointName','pointState','pointCity','pointMunicipality','pointParish','pointAddress'] 
            : form === 'login' 
            ? ['login','password',] : [])
        ].reduce((all,key) => all = { ...all, [key]: useRef(null) }, {})
    };
    const token = useSelector((state) => state.auth.token);
    const [textInputsArr, setTextInputsArr] = useState([...fieldsArray].filter(field => field.type === 'input').map(fld => fld.handler));
    const dispatch = useDispatch();

    useEffect(() => {
        setTimeout(() => inputRefs[Object.keys(inputRefs)[0]].current.focus(), 1)
    },[]);

    return (
        <Formik
            initialValues={
                // POINT
                form === 'point' ? { 
                    pointName: '', 
                    ...['State','City','Municipality','Parish','Address']
                        .reduce((keys, key) => keys = { ...keys, [`point${key}`]: formFields[`form${key}`] } , {}),
                    pointType: Platform.OS === 'ios' ? null : '01',
                    pointSize: Platform.OS === 'ios' ? null : '1',
                    ...(Platform.OS === 'ios' ? {
                        pointTypeInput: 'Seleccione',
                        pointSizeInput: 'Seleccione',
                    } : {}),
                    ...['Chain','Caught'].reduce((keys, key) => keys = { ...keys, [`point${key}`]: '0' } , {}),
                } 
                : 
                // LOGIN
                form === 'login' ? ['login','password'].reduce((keys, key) => keys = { ...keys, [key]: '' } , {}) : {}
            }
            validationSchema={validations(form, Platform.OS === 'ios')}
            onSubmit={async (values, formikActions) => {
                console.log(values);
                const data = dataConstructor(values, formFields, token, form);
                const request = await apiFetch(
                    form === 'point' ? 'general' : 'users', 
                    form === 'point' ? 'registerSalePoint' : 'login', 
                    data
                );

                if (request.response) {
                    console.log('log response', request.response);
                    formikActions.setSubmitting(false);
                    setModalActionsEnabled(true);
                    handleAction(...(
                        form === 'point' ? [ {
                            msg: request.response.mensaje.descripcion,
                            coords: formFields.formCoords,
                            title: data.nombrePV || 'Área registrada',
                        } ] : 
                        form === 'login' ? [ request.response.datos ] : 
                        []
                    ));
                } else {
                    console.error('log error', typeof request.error === 'object' && request.error.sessionNotActive ? request.error.msg : request.error);
                    formikActions.setSubmitting(false);
                    setModalActionsEnabled(true);
                    Alert.alert( 
                        ...(typeof request.error === 'object' && request.error.sessionNotActive ? [
                            `${request.error.msg}`, '', [{ text: 'OK', onPress: () => executeCloseSession(dispatch, navigation) }]
                        ] : [`${request.error}`])
                    );
                }
            }}
        >
            {({ handleChange, handleBlur, handleSubmit, setFieldValue, setTouched, isSubmitting, values, touched, errors }) => (
                <>
                <ScrollView
                    contentInsetAdjustmentBehavior="automatic"
                >
                    {formFields.formCoords ? <View>
                        <Text style={styles.titleInModal}>Coordenadas del punto</Text>
                        {['atitud','ongitud'].map(lbl => <Text key={lbl} style={{ color: styles.titleInModal.color }}>L{lbl}: {formFields.formCoords[`l${lbl}e`]}</Text>)}
                    </View> : <></>}

                    {
                    fieldsArray.map((field, fi, farr) => 
                        <View key={fi} style={styles.separatorTop}>
                            <Text style={styles.titleInModal}>{field.label}</Text>
                            {field.type === 'input' ? 
                                <>
                                <TextInput
                                    ref={inputRefs[field.handler]}
                                    editable={!isSubmitting}
                                    // autoFocus={!fi}
                                    placeholder={field.label}
                                    style={styles.inputInModal}
                                    onChangeText={handleChange(field.handler)}
                                    onBlur={handleBlur(field.handler)}
                                    value={values[field.handler]}
                                    secureTextEntry={field.handler === 'password'}
                                    onSubmitEditing={() => {
                                        let deleted;
                                        const inx = textInputsArr.indexOf(field.handler);
                                        if (inx > -1) deleted = textInputsArr.splice(inx, 1);
                                        for (let f = 0; f < textInputsArr.length; f++) {
                                            const passer = !values[field.handler] || !values[textInputsArr[f]];
                                            if (passer) {
                                                if (!values[field.handler]) {
                                                    inputRefs[field.handler].current.blur();
                                                    inputRefs[field.handler].current.focus();
                                                }
                                                else inputRefs[textInputsArr[f]].current.focus();
                                                break;
                                            }
                                        }
                                        if (deleted && deleted.length) setTextInputsArr([
                                            ...textInputsArr,
                                            ...deleted,
                                        ]);
                                    }}
                                />
                                {touched[field.handler] && errors[field.handler] ? <Text style={styles.errorMsg}>{errors[field.handler]}</Text> : <></>}
                                </>
                                : field.type === 'select' ? 
                                <PickerComponent 
                                    field={field}
                                    values={values}
                                    touched={touched}
                                    errors={errors}
                                    isSubmitting={isSubmitting}
                                    setFieldValue={setFieldValue}
                                />
                                : // radio buttons
                                <View style={styles.radioContainer}>
                                    {field.list.map((itm, i) => 
                                    <RadioButton 
                                        key={i}
                                        id={itm.id}
                                        disabled={isSubmitting}
                                        label={itm.label}
                                        labelStyle={{ color: styles.inputInModal.color }}
                                        selected={values[field.handler] == itm.id}
                                        onPress={(id) => setFieldValue(field.handler, id)}
                                    />)}
                                </View>
                            }
                        </View>  
                    )
                    }
                </ScrollView>

                <Pressable 
                    style={[styles.separatorTop, styles.btnInModal, { backgroundColor: isSubmitting ? COLORS.blue[1] : COLORS.blue[0] }]}
                    disabled={isSubmitting}
                    onPress={() => {
                        const errorsArr = Object.keys(errors);
                        if (!errorsArr.length) {
                            setModalActionsEnabled(false);
                            handleSubmit();
                        }
                        else if (inputRefs[errorsArr[0]]) {
                            inputRefs[errorsArr[0]].current.blur();
                            inputRefs[errorsArr[0]].current.focus();
                        }
                        else if (Platform.OS === 'ios' && errorsArr.some(err => err.includes('Input'))) 
                            setTouched({ ...touched, ...errorsArr.reduce((errs, err) => errs = { ...errs, [err]: true }, {}) });
                    }}
                ><View style={styles.rowCenter}>
                    {isSubmitting ? <>
                        <ActivityIndicator size="small" color="#ffffff" />
                        <View style={styles.separatorInner}></View>
                    </> : <></>}
                    <Text style={styles.textInBtn}>{btnLabel}</Text>
                 </View>
                </Pressable>
                </>
            )}
        </Formik>
    );
};

const styles = styles_();