import React from "react";
import { Modal, Platform, Pressable, Text, View } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { styles_ } from "../assets/styles/GlobalStyles";

export const ModalComponent = ({
    children,
    formModalVisible,
    actionsEnabled = true,
    setFormModalVisible = () => {},
}) => {
    const insets = useSafeAreaInsets();
  
    return (
        <Modal
          animationType="slide"
          transparent={true}
          visible={formModalVisible}
          onRequestClose={() => {
            setFormModalVisible(!formModalVisible);
          }}
        >
          <View style={styles.centeredView}>
            <View style={{
              ...styles.modalView, 
              ...(Platform.OS === 'ios' ? { 
                marginTop: insets.top + 7.5,
                marginBottom: insets.bottom + 7.5,
                } : {})
            }}>
              <View style={styles.modalViewHeader}>
                <Pressable disabled={!actionsEnabled} onPress={() => setFormModalVisible(!formModalVisible)}>
                  <Text style={styles.timesCloser}>&times;</Text>
                </Pressable>
              </View>
              <View style={styles.modalViewBody}>
                {children || <></>}
              </View>
            </View>
          </View>
        </Modal>
    );
};

const styles = styles_();