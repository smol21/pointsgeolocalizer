import React, { useState } from "react";
import {
    SafeAreaView,
    StatusBar,
    Text,
    useColorScheme,
    View,
} from 'react-native';
import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';
import { useDispatch } from "react-redux";
import LoginFormFields from '../assets/LoginFormFields';
import { styles_ } from "../assets/styles/GlobalStyles";
import { FormComponent } from '../components/FormComponent';
import { saveToken } from "../store/reducers";

export const LoginScreen = ({
    navigation
}) => {
    const dispatch = useDispatch();
    const [formLogin, setFormLogin] = useState('');
    const [formPassword, setFormPassword] = useState('');
    const isDarkMode = useColorScheme() === 'dark';

    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };

    const goHome = (token) => {
        dispatch(saveToken(token));
        navigation.reset({
            index: 0,
            routes: [{ name: /* 'Drawer' */'Home' }],
        });
    };

    return (
        <SafeAreaView style={backgroundStyle}>
            <StatusBar
                barStyle={isDarkMode ? 'light-content' : 'dark-content'}
                backgroundColor={backgroundStyle.backgroundColor}
            />
            <View style={styles.loginContainer}>
                <Text style={styles.bigLabel}>Inicio de Sesión</Text>
                <View>
                    <FormComponent
                        formFields={{
                            formLogin,
                            formPassword,
                        }}
                        fieldsArray={LoginFormFields}
                        form="login"
                        btnLabel="Iniciar Sesión"
                        handleAction={goHome}
                    />
                </View>
            </View>
        </SafeAreaView>
    );
};

const styles = styles_();