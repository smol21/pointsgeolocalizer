import React, { useEffect, useState } from 'react';
import {
  SafeAreaView,
  StatusBar,
  Text,
  View,
  Pressable,
  useColorScheme,
  Alert,
  Platform,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
import { COLORS, DURATION, styles_ } from '../assets/styles/GlobalStyles';
import { dataFromGeocoding, hasLocationPermission } from '../assets/Utils';
import { MapComponent } from '../components/MapComponent';
import { FormComponent } from '../components/FormComponent';
import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import PointFormFields from '../assets/PointFormFields';
import { ModalComponent } from '../components/ModalComponent';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { clearToken } from '../store/reducers';
import { useDispatch } from 'react-redux';

export const HomeScreen = () => {
  Geocoder.init("AIzaSyBf85VduFQMSupstCbVyMF3M7VAevp7yLU");
  const dispatch = useDispatch();
  const insets = useSafeAreaInsets();
  const [location, setLocation] = useState(null);
  const [gettingLocation, setGettingLocation] = useState(false);
  const [gettingGeocoding, setGettingGeocoding] = useState(false);
  const [formState, setFormState] = useState('');
  const [formMunicipality, setFormMunicipality] = useState('');
  const [formCity, setFormCity] = useState('');
  const [formParish, setFormParish] = useState('');
  const [formAddress, setFormAddress] = useState('');
  const [formModalVisible, setFormModalVisible] = useState(false);
  const [formCoords, setFormCoords] = useState(null);
  const [didIntersect, setDidIntersect] = useState(false);
  const [circles, setCircles] = useState([
    // {
    //   longitude: (-69.3541405 - 0.0006),
    //   latitude: 10.077563,
    //   radius: 30,
    //   info: {
    //     name: 'Marcador de ejemplo',
    //   }
    // },
  ]); 
  const [theStrokeColor, setTheStrokeColor] = useState(COLORS.green[1] || COLORS.green[0]);
  const [inhabilitateRegisterPoint, setInhabilitateRegisterPoint] = useState(false);
  const [modalActionsEnabled, setModalActionsEnabled] = useState(true);

  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const getLocation = async () => {
    const hasPermission = await hasLocationPermission(Geolocation);

    if (!hasPermission) {
      return;
    }
    setGettingLocation(true);
    Geolocation.getCurrentPosition(
      (position) => {
        setLocation(position);
        setGettingLocation(false);
      },
      (error) => {
        setLocation(null);
        setGettingLocation(false);
        console.log(error.code, error.message);
      },
      {
        accuracy: {
          android: 'high',
          ios: 'best',
        },
        enableHighAccuracy: true,
        timeout: 15000,
        maximumAge: 10000,
        distanceFilter: 0, // optional
        forceRequestLocation: true, // optional
        forceLocationManager: false, // optional
        showLocationDialog: true, // optional
      },
    );
  };

  const geocodeFrom = (region) => {
    if (!gettingGeocoding && region?.center && !didIntersect) {
      setFormCoords(region.center);
      setGettingGeocoding(true);
      // 
      setTheStrokeColor(COLORS.green[1] || COLORS.green[0]);
      setInhabilitateRegisterPoint(false);
      // 
      Geocoder.from({
        latitude : region.center.latitude,
        longitude : region.center.longitude
      })
      .then(json => {
        if (json.status === "OK") {
          const formData = dataFromGeocoding(json);

          if (formData.state) setFormState(formData.state);
          if (formData.municipality) setFormMunicipality(formData.municipality);
          if (formData.city) setFormCity(formData.city);
          if (formData.parish) setFormParish(formData.parish);
          if (formData.sublocality || formData.route) setFormAddress(`${formData.route ? (`${formData.route}`+`${formData.sublocality ? ', ':''}`) : ''}${formData.sublocality || ''}`);
        }
        setGettingGeocoding(false);
      })
      .catch(error => {
        console.warn(error);
        setGettingGeocoding(false);
      });
    }
  };
 
  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <View style={styles.container}>
        <MapComponent 
          location={location} 
          isDarkMode={isDarkMode} 
          circles={circles}
          getStrokeColor={theStrokeColor}
          onSetDidIntersect={setDidIntersect}
          callGeocodeFrom={geocodeFrom}
          callLocation={getLocation}
        />
      </View>
      <View style={styles.floatingBtnContainer}>
        <Pressable
          style={{
            ...styles.floatingBtn, 
            backgroundColor: gettingLocation ? COLORS.blue[1] : COLORS.blue[0],
            ...(Platform.OS === 'ios' ? {
              marginBottom: insets.bottom + 8,
            } : {})
          }}
          onPress={() => { 
            if (!gettingLocation) getLocation(); 
            // dispatch(clearToken());
          }}
          disabled={gettingLocation}
        >
          <Text style={styles.whiteColor}>Refrescar Ubicación</Text>
        </Pressable>

        <Pressable
          style={{
            ...styles.floatingBtn, 
            backgroundColor: inhabilitateRegisterPoint || gettingLocation ? COLORS.black[1] : COLORS.black[0], 
            ...(didIntersect ? { backgroundColor: COLORS.red[1] || COLORS.red[0] } : {}),
            ...(Platform.OS === 'ios' ? {
              marginBottom: insets.bottom + 8,
            } : {})
          }}
          onPress={() => setFormModalVisible(true)}
          disabled={(location && didIntersect) || inhabilitateRegisterPoint || gettingLocation}
        >
          <Text style={styles.whiteColor}>Registrar Punto</Text>
        </Pressable>
      </View>
      
      <View style={styles.centeredView}>
        <ModalComponent
          actionsEnabled={modalActionsEnabled}
          formModalVisible={formModalVisible}
          setFormModalVisible={setFormModalVisible}
        >
          <FormComponent
            formFields={{
              formCoords,
              formState,
              formMunicipality,
              formCity,
              formParish,
              formAddress,
            }}
            fieldsArray={PointFormFields}
            btnLabel="Enviar"
            setModalActionsEnabled={setModalActionsEnabled}
            handleAction={(resObj) => { 
              setFormModalVisible(false); 
              // COMENTADOS DE MOMENTO, SERA USADO EN FUTURAS IMPLEMENTACIONES
              /* setCircles([...circles, {
                longitude: resObj.coords.longitude,
                latitude: resObj.coords.latitude,
                radius: 30,
                info: {
                  name: resObj.title,
                }
              }]);  */
              // setInhabilitateRegisterPoint(true);
              setTheStrokeColor(COLORS.black[1] || COLORS.black[0]);
              Alert.alert(resObj.msg);
            }}
          />
        </ModalComponent>
      </View>
    </SafeAreaView>
  );
};

const styles = styles_();