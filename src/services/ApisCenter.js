const apiUrl = 'https://apismol.com.ve/';
    // 'http://200.44.165.134:8041/';
    // 'http://192.168.200.226:8041/';

const services = {
    general: 'servicegeneral/ServicioGeneral.svc/ServicioGeneral/',
    users: 'serviceusers/ServicioUsuarios.svc/ServicioUsuario/',
};

const endpoints = {
    login: 'IniciarSesionAppGeo',
    registerSalePoint: 'RegistrarPuntoVenta',
};

export const apiFetch = async (service, endpoint, data = null) => {
    let msg;
    try {
        const raw_response = await fetch(...[
            `${apiUrl}${services[service]}${endpoints[endpoint]}`,
            ...(data ? [{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            }] : []),
        ]);
        msg = ((raw_response?.status || '') + ' ' + (raw_response?.statusText || '')).trim();
        const response = await raw_response.json();
        
        if (response?.mensaje?.codigo !== '000') {
            msg = (response.mensaje.codigo === '001' ? { sessionNotActive: true, msg: (response.mensaje.descripcion || '') } : '') || response.mensaje.descripcion || response.mensaje.codigo;
            throw new Error(msg);
        }

        return { response }
    } 
    catch (error) {
        return { 
            error: msg || error
        }
    }
}