/**
 * @format
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { AppNavigator } from './src/containers/AppNavigator';
import { persistStore } from 'redux-persist'
import { store } from './src/store/configureStore';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import BootSplash from "react-native-bootsplash";

let persistor = persistStore(store);

function App(): React.JSX.Element {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer
          onReady={() => {
            BootSplash.hide();
          }}
        >
          <AppNavigator/>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}

export default App;
